def call(serviceName, imageName) {
    pipeline {
        agent any

        parameters {
            booleanParam(defaultValue: false, description: 'Deploy the App', name:'DEPLOY')
        }

        stages {
            stage('Lint') {
                steps {
                    sh "echo ${serviceName}"
                    sh "echo ${imageName}"
                    sh "pip install -r ${serviceName}/requirements.txt --break-system-packages"
                    sh "pylint --fail-under 5.0 ${serviceName}/*.py"
                }
            }

//            stage('Security') {
//                steps {
//                    script {
//                        // Install Safety
//                        sh "pip install safety --break-system-packages"
//
//                        // Run Safety against Python dependencies with full path
//                        sh "/var/jenkins_home/.local/bin/safety check --full-report --file=${serviceName}/requirements.txt"
//                    }
//                }
//            }

            stage('Package') {
                when {
                    expression { env.GIT_BRANCH == 'origin/main' }
                }
                steps {
                    script {
                        // Build the Docker image and push it to Dockerhub
                        withCredentials([string(credentialsId: 'DockerHub', variable: 'TOKEN')]) {
                            sh "docker login -u 'goutamthukral' -p '$TOKEN' docker.io"
                            sh "docker build -t goutamthukral/${serviceName}:latest ${imageName}/."
                            sh "docker push goutamthukral/${serviceName}:latest"
                        }
                    }
                }
            }

            stage('Deploy') {

                when {
                    expression { params.DEPLOY }
                }
                
                steps {
                    script {
                        sshagent(credentials: ['cloudVM']) {
                            sh """
                                ssh -o StrictHostKeyChecking=no azureuser@65.52.126.126 '
                                cd .. && 
                                cd goutam_thukral/acit3855/deployment &&
                                docker pull goutamthukral/${imageName}:latest &&
                                docker-compose up -d '
                            """
                        }
                    }
                }
            }
        }
    }
}
